# MISCELANEOS

    Este proyecto se desarrolla lo siguiente:

    - ngStyle  --> Nos permite realizar cambios en caliente a nuestros estilos
    <some-element [ngStyle]="{'font-style': styleExp}">...</some-element>

    - ngClass  --> Es otra directiva, para trabajar mejor con ngStyle. Nos permite hacer un append al atributo class.

    - directivas Personalizadas --> Creacion y aplicaion de directivas personalizadas.

    - ngSwitch --> Evaluacion

    - Rutas y Rutas hijas   --> Obteniendo parámetros del padre, desde las rutas hijas

    - Ciclo de vida de un Componente.
        *   ngOnInit                --> Cuando el componente esta inicializando
        *   ngOnChanges             --> Cunado la data de propiedades relacionadas cambian
        *   ngDoCheck               --> Durante cada revision del ciclo de deteccion de cambios
        *   ngAfterContentInit      --> Despues de insertar el contenido (<app-alguna-pagina>)
        *   ngAfterContentCheked    --> Despues de la revision del contenido insertado
        *   ngAfterViewInit         --> Despues de la inicializacion del componente/hijos
        *   ngAfterViewChecked      --> Despues de cada revision de los componentes / hijos
        *   ngOnDestroy             --> Justo antes que se destruya el componente o directiva
    





    





##  Creando el proyecto
    ng g c components/ng-style -it -is

    -it --> para crear el template dentro de archivo 
    -is --> para crear el style dentro del archivo


## Crenado Directivas Personalizadas
    ng g d directives/resaltado

## ngSwitch
    tag rapido rapido para escribir `ngSwitch` en un Template es: ngS...
    

## Tips 
    Creando compononentes hijos sobre una mismas carpeta
    
    ng g c components/user/userDetail -is -it --flat

