// import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { UserDetailComponent } from './user-detail.component';
import { UserEditComponent } from './user-edit.component';
import { UserNewComponent } from './user-new.component';

export const USUARIO_ROUTES: Routes = [
    { path: 'new', component: UserNewComponent },
    { path: 'edit', component: UserEditComponent },
    { path: 'detail', component: UserDetailComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'edit' }
];
