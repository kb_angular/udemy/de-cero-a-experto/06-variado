import { Component, OnInit } from '@angular/core';
// Captura lo que se esta enviandoi por link
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: [
  ]
})
export class UserComponent implements OnInit {

  constructor( private activatedRoute: ActivatedRoute ) {

    this.activatedRoute.params.subscribe( params => {
      console.log("Ruta-Padre - Usuario");
      console.log(params);
    });
  }

  ngOnInit(): void {
  }

}
