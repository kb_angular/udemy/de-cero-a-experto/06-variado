import { Directive,ElementRef, HostListener, Input } from '@angular/core';
import { ConstantPool } from '@angular/compiler';


@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  constructor(private el: ElementRef) {
    console.log('Directiva llamada');
  }

  // Preparado para que selector tenga nuevos valores.
  @Input("appResaltado") nuevoColor: string;

  // Se activa cuando el mouse entra en el elemento
  @HostListener('mouseenter') mouseEntro(){
    this.cambiar(this.nuevoColor || 'yellow');
    
  }

  // Se activa cuando el mouse sele del el elemento
  @HostListener('mouseleave') mouseSalir(){
    this.cambiar(null);
  }

  cambiar(color: string){
    this.el.nativeElement.style.backgroundColor = color;
  }



}
