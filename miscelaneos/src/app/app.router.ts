import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
import { USUARIO_ROUTES } from './components/user/user.router';

const routes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'user/:id', component: UserComponent,
      children: USUARIO_ROUTES
    },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
